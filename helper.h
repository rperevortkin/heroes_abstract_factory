#ifndef HELPER_H
#define HELPER_H

#include <QObject>
#include <QQmlEngine>

#include "herofactory.h"

class Helper : public QObject
{

    Q_OBJECT
public:
    Helper(QObject *parent = nullptr) : QObject(parent) {}


    Q_INVOKABLE HeroFactory *getFactory(QString type) {
        HeroFactory *factory;
        if (type == "good") {
            factory = new GoodHeroFactory();
        } else {
            factory = new EvilHeroFactory();
        }
        return factory;
    }

    static QObject *instance(QQmlEngine *engine, QJSEngine *scriptEngine) {

        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        static Helper *helper;
        if (!helper) {
            helper = new Helper();
        }
        return helper;
    }

};
#endif // HELPER_H
