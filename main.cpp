#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "abstracthero.h"
#include "casterhero.h"
#include "warriorhero.h"
#include "herofactory.h"
#include "helper.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterUncreatableType<AbstractHero>("Heroes", 1, 0, "AbstractHero", "can not instantiate AbstractHero in qml");
    qmlRegisterUncreatableType<HeroFactory>("Heroes", 1, 0, "HeroFactory", "can not instantiate HeroFactory in qml");
    qmlRegisterSingletonType<Helper>("Heroes", 1, 0, "Helper", &Helper::instance);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
