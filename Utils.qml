import QtQuick 2.7


Item {
    readonly property url goodHero: "assets/good.jpeg"
    readonly property url evilHero: "assets/evil.jpeg"
    readonly property url goodWizard: "assets/wizard.jpg"
    readonly property url goodPaladin: "assets/paladin.jpg"
    readonly property url evilNecromancer: "assets/necromancer.jpg"
    readonly property url evilBarbarian: "assets/barbarian.jpg"

    function resolveSource(character) {
        var icon = "";
        switch (character) {
        case "Wizard":
            icon = goodWizard;
            break;
        case "Paladin":
            icon = goodPaladin;
            break;
        case "Necromancer":
            icon = evilNecromancer;
            break;
        case "Barbarian":
            icon = evilBarbarian;
            break;
        default:
            console.warn("Unhandled character : " + character);
        }
        return icon;
    }
}
