#ifndef HEROFACTORY_H
#define HEROFACTORY_H

#include <QObject>
#include <QQmlEngine>

#include "abstracthero.h"
#include "casterhero.h"
#include "warriorhero.h"

class HeroFactory : public QObject
{
    Q_OBJECT
public:
    HeroFactory(QObject *parent = nullptr) : QObject(parent) {}
    virtual Q_INVOKABLE AbstractHero *createCaster() = 0;
    virtual Q_INVOKABLE AbstractHero *createWarrior() = 0;
};

class GoodHeroFactory : public HeroFactory
{

public:
    GoodHeroFactory(QObject *parent = nullptr) : HeroFactory(parent) {}
    Q_INVOKABLE AbstractHero *createCaster() { AbstractHero *hero = new CasterHero("Gandalf", "Fireball");
                                               QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                               return hero; }
    Q_INVOKABLE AbstractHero *createWarrior() { AbstractHero *hero = new WarriorHero("Aragorn", "Sword attack");
                                                QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                return hero; }
};

class EvilHeroFactory : public HeroFactory
{

public:
    EvilHeroFactory(QObject *parent = nullptr) : HeroFactory(parent) {}
    Q_INVOKABLE AbstractHero *createCaster() { AbstractHero *hero = new CasterHero("Saruman", "Evil army");
                                               QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                               return hero; }
    Q_INVOKABLE AbstractHero *createWarrior() { AbstractHero *hero = new WarriorHero("Ork", "Baton attack");
                                                QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                return hero; }
};
#endif // HEROFACTORY_H
