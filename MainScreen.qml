import QtQuick 2.0

import QtGraphicalEffects 1.0

Item {
    id: _main
    anchors.fill: parent

    ListModel { id: _heroModel }

    Text {
        id: _headerText
        anchors.bottom: _icon.top
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter

        font.pixelSize: 30
    }

    Image {
        id: _icon

        width: 300
        height: 300

        anchors.leftMargin: 50
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 100
    }

    ListView {
        id: _list

        anchors.left: _icon.right
        anchors.leftMargin: 50
        anchors.verticalCenter: parent.verticalCenter

        Keys.onEscapePressed: {
            _main.state = "selectCharacter";
        }
        spacing: 10
        height: 210
        highlightMoveDuration: 10
        highlightMoveVelocity: 10

        focus: true
        interactive: false

        highlight: Rectangle {
            radius: 5
            y: _list.currentItem.y

            RectangularGlow {
                anchors.fill: parent
                glowRadius: 10
                spread: 0.2
                color: Qt.darker(_list.currentItem.delegateColor)
                cornerRadius: parent.radius + glowRadius
            }
        }

        delegate: Delegate {}
    }

    ListView {
        id: _heroList

        anchors.top: _icon.bottom
        anchors.leftMargin: 20
        anchors.left: _main.left
        anchors.right: _main.right
        anchors.bottom: _main.bottom

        interactive: false
        orientation: ListView.Horizontal

        model: _heroModel
        delegate: Item {
            width: 150
            height: 150

            Text {
                id: _text
                anchors.centerIn: parent
                text: model.hero.name
            }
            Image {
                anchors.top: _text.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                width: 100
                height: 100
                source: _utils.resolveSource(model.character)
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    _heroModel.remove(index);
                }
            }
        }
    }

    state: "selectCharacter"

    states: [
        State {
            name: "selectCharacter"
            PropertyChanges {
                target: _list
                model: ["Good hero", "Evil hero"]
            }
            PropertyChanges {
                target: _icon
                source: _list.currentIndex === 0 ? _utils.goodHero : _utils.evilHero
            }
            PropertyChanges {
                target: _headerText
                text:  qsTr("Select your character")
            }
        },
        State {
            name: "selectGoodHero"
            PropertyChanges {
                target: _list
                model: ["Wizard", "Paladin"]
            }
            PropertyChanges {
                target: _icon
                source: _utils.resolveSource(_list.currentItem.character)
            }
            PropertyChanges {
                target: _headerText
                text:  qsTr("Select your good hero")
            }
        },
        State {
            name: "selectEvilHero"
            PropertyChanges {
                target: _list
                model: ["Necromancer", "Barbarian"]
            }
            PropertyChanges {
                target: _icon
                source: _utils.resolveSource(_list.currentItem.character)
            }
            PropertyChanges {
                target: _headerText
                text:  qsTr("Select your evil hero")
            }
        }
    ]

}
