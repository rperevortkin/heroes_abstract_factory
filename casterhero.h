#ifndef CASTERHERO_H
#define CASTERHERO_H

#include "abstracthero.h"

class CasterHero : public AbstractHero
{

public:
    CasterHero(QObject *parent = nullptr) : AbstractHero(parent) {}
    CasterHero(QString name, QString hit,QObject *parent = nullptr) :
        AbstractHero(parent) { m_name.append(name);
                               m_hit.append(hit); }

    QString name() const { return m_name; }
    QString hit() const { return m_hit; }

private:
    QString m_name = "Hero caster ";
    QString m_hit = "Caster hit ";
};

#endif // CASTERHERO_H
