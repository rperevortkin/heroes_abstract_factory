import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQml.Models 2.2

import Heroes 1.0

Item {
    id: root

    scale: _mouseArea.pressed ? 0.95 : 1.0
    width: 200
    height: 100

    readonly property color delegateColor: isGoodCharacter ? "lightgreen" : "#E45252"
    readonly property bool isGoodCharacter: character === "Good hero" || character === "Wizard" || character === "Paladin"
    readonly property string character: model.modelData

    Rectangle {
        anchors.centerIn: parent
        width: 180
        height: 90
        radius: 5
        color: delegateColor
    }
    Text {
        anchors.centerIn: parent

        font.pixelSize: 20
        text: model.modelData
    }

    MouseArea {
        id: _mouseArea

        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            if (_list.currentIndex !== index) {
                _list.currentIndex = index;
            }
        }
        onPressed: {
            if (_main.state === "selectCharacter") {
                _main.state = _list.currentIndex === 0 ? "selectGoodHero" : "selectEvilHero";
            } else {
                if (_heroModel.count === 4) return;
                var factory = Helper.getFactory(isGoodCharacter ? "good" : "evil");
                var hero;
                if (character === "Wizard" || character === "Necromancer") {
                    hero = { hero : factory.createCaster(),
                             character: root.character };
                } else {
                    hero = { hero : factory.createWarrior(),
                             character: root.character };
                }
                 _heroModel.append(hero);
            }
        }
    }
}
