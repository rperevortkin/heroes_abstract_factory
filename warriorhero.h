#ifndef WARRIORHERO_H
#define WARRIORHERO_H

#include "abstracthero.h"

class WarriorHero : public AbstractHero
{

public:
    WarriorHero(QObject *parent = nullptr) : AbstractHero(parent) {}
    WarriorHero(QString name, QString hit, QObject *parent = nullptr) :
        AbstractHero(parent) { m_name.append(name);
                               m_hit.append(hit); }

    QString name() const { return m_name; }
    QString hit() const { return m_hit; }

private:
    QString m_name = "Hero warrior ";
    QString m_hit = "Warrior hit ";
};

#endif // WARRIORHERO_H
